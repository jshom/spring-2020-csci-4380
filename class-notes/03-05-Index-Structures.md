# Index Structures Continued

## Hash Tables

A hash table is a data structure that organizes data using a hash function, which takes input and returns an integer between 0 and B-1, where B is the number buckets in the hashtable. 

A bucket array is an array, indexed from 0 to B-1 that holds the heads of linked lists. 

If a record as search key K, we store it in the bucket list for the bucket numbered H(K)

### Secondary Storage Hash Tables

A hash table may hold too many records to fit in main memory. If the records are kept in secondary storage, we make a few alterations from a "normal" hashtable

The bucket array consists of Blocks rather than Linked Lists

Assume that the location for the first block in any bucket can be determined (maybe a main memory array of pointers to blocks, or the buckets are in fixed, consecutive locations on disk)

Each block holds records and a "nub" with additional information, like pointers to overflow blocks 

#### Insertion

Compute H(k), determine the block, if there's no space in the block, add an overflow block

#### Deletion

Similar to insertion. If we can consolidate overflow blocks, we do

### Choice of Hash Function

The hash function should produce an even distribution of keys over the range of the function 

It should be easy to compute

It's a common choice for integers to use `k % B` 

### Efficiency

Ideally, there are enough buckets that most of them fit on one block. Then lookup takes only 1 I/0. Significantly better than B-Tree

However, as the file grows, we'll end up with multiple overflow blocks, potentially long lists of blocks (many I/Os per lookup). A good reason to try and keep the blocks/bucket low

## Dynamic Hashtables

Dynamic tables can expand as the amount of data grows

Two approachs:
- Extensible Hash Tables
- Linear Hash Tables

### Extensible Hash Tables

4 Major differences between static hash tables and extensible hash tables:
1. A level of indirection for buckets: The array of pointers is to buckets, not blocks themselves
2. The array can grow in length. It's always a power of 2
3. There doesn't have to be a block for each bucket (buckets can share blocks under certain circumstances)
4. The hash function computes a sequence of k bits, for some large value of k (maybe 32)
    - However, only the first i bits are used.
    - The array is of length 2^i

#### Insertion into an extensible Hashtable

1. Compute H(k) for the search key k
2. Consider the first i bits, go to the bucket for that value
3. Arrive at block B
    - If there's space, insert the record, and we're done
    - If there's not space, we have two options, depending on the value of j
        - If j < i: Split B into 2 blocks
            - Distribute the records in B based on the value of their (j+1) bit
            - Put (j+1) in each block's nub
            - Adjust the pointers in the bucket array accordingly 
        - If j = i: Increment i
            - Double the length of the bucket array so it now has 2^(i+1) entries
            - Given some w determined by the first i bits, there are now w0 and w1. Both will point to the same block as w
            - Now j < i, and we can split the block as in the previous case

![extensible-hashtable](resources/03-05/extensible-hashtable.jpg)

**Advantages of Extensible Hashtables**:

- Never have to retrieve more than one block 

**Disadvantages**:
- Doubled array may no longer fit in memory, or may crowd out other data we'd want in memory
- If the number of records per block is small, we're likely to see a block that is split far earlier than necessary

### Linear Hashing

It grows the buckets more slowly

Differences/New Elements:
- The number of buckets n is always chosen so that the average number of records per block is a fixed fraction (e.g., 80%)
- Blocks cannot be split, so overflow blocks are permitted
    - The number of overflows per bucket will average < 1
- The number of bits used to determine the entries of the bucket array is Log2(n), where n is the current number of buckets. These bits are always taken from the right hand side (least significant) 
- Suppose i bits are being used to number array entries:
    - a record with key k is intended for bucket a1a2...ai (the last i bits)
    - Treat a1a2...ai as integer m
    - If m < n, the bucket must exist: we insert there 
    - If m <= n < 2^i, the bucket doesn't exist yet, we use the bucket represented by m-2^(i-1)
- Each time we do an insert, we compare r/n to our desired ratio 
    - If it's too high, we add another bucket to the table 
    - Note that the new bucket likely has no relation to the inserted record
- If the number of buckets exceeds 2^i, increment i
    - All buckets now have a leading 0, but it doesn't matter, since we treat them as integers anyway
    

## Multidimensional Indexes

All indexes so far are one-dimensional

They may include multiple attributes but don't function well if an attribute is missing or unknown 

### Applications of Multidimensional Indexes

Often not done in SQL systems

One important application is Geographic Information Systems 
- Obviously used for maps
- Integrated circuit design
- UX layout

Typical Queries we ask of a GIS:
1. Partial Match queries: specify values for a subset of attributes: look for all matches 
2. Range Queries: given ranges for one or more dimensions, find all points within the ranges 
3. Nearest Neighbor: Ask for the closest point to a given point
4. Where Am I? queries: given a point, what shapes contain that point

Conventional indexes will support these to an extent:

Range queries: B-Tree index on each dimension. 
    - Execute the range query for each, and obtain a set of pointers, take the intersection of those sets

Nearest Neighbor: we pick a range in each dimension and execute a query

Failure points:
- No point in range 
- Closer point is actually outside 

We can address the first by gradually expanding the range

Address the second by executing the query again with a slightly larger range to confirm 

## Overview of Multidimensional Structures

Similar to one-dimensional index, structures fall into two categories:
- Hashtable-like structures
- Tree-like Structures

In both cases, we give something up to gain the added dimension:

Hash structures will not answer queries with just one bucket

Tree structures give up at least one of:
- Balance of the tree
- Correspondence between tree nodes and disk blocks 
- Speed of modifications

### Hash Structures

**Grid Files**: in each dimension, the space is partitioned into stripes

The number of grid lines may vary between dimensions, and the spacing between them might not always be equal

The idea is to set the grid lines so that there are an equal number of data points in each space

![grid file](resources/03-05/grid-file.jpg)

**Partitioned Hash Function**

Hash functions can take multiple arguments
We can design a hash function so that it's actually a series of hash functions 

For example, we could use 10 bits: 4 bits for the first argument, and 6 bits for the second

This allows us to limit the buckets of the hash table in which we look 

### Tree-like Structures

- Multiple-key indexes
- kd-trees
- quad trees
- R-trees 

**Multiple Key indexes**

Basic idea is to build an index of indexes 

Partial match queries work well if we have the first attribute. Otherwise, we have to search every sub-index. 

Range queries and nearest neighbor queries work reasonably well

**kd-trees**

A kd-tree is a binary tree where interior nodes have an attribute and a value V for that attribute that evenly splits the dataset in two.

The attributes at different levels are different, alternating as needed among the dimensions

![kd-tree](resources/03-05/kd-tree.jpg)
