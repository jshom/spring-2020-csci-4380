import psycopg2

import psycopg2.extras

conn_string = "host='localhost' dbname='example_db' user='example_user' password='example-password'"

conn = psycopg2.connect(conn_string)
# cursor = conn.cursor()
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


# cursor.execute("SELECT * FROM course")
# query = "SELECT * FROM course WHERE name=%(name)s AND capacity>%(capacity)s"
# cursor.execute(query, {'name': 'Database Systems', 'capacity': 20})
#
# records = cursor.fetchall()
#
#
# for t in records:
#     print(t)


update_query = "INSERT INTO enroll(student_email, course_name, semester) VALUES ('g@example.com', 'Database Systems', 'S20')"

cursor.execute(update_query)
conn.commit()

check_query = "SELECT * FROM enroll"
cursor.execute(check_query)

records = cursor.fetchall()
for t in records:
    print(t)

