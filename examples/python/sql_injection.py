import psycopg2
import psycopg2.extras

conn_string = "host='localhost' dbname='example_db' user='example_user' password='example-password'"

conn = psycopg2.connect(conn_string)
cursor = conn.cursor()


capacity = 30
# course_name = 'Database Systems'
course_name = "Data'; SELECT * FROM grades WHERE student_email <> '"
semester = 'S20'


# Bad. DON'T DO THIS

query = "SELECT * FROM course WHERE semester='" + semester + "' AND name='" + course_name + "'"

"""
SELECT *
FROM course
WHERE semester='S20'
AND name='Data'; SELECT * FROM grades WHERE student_email <> ''
"""

# query = "SELECT * FROM course WHERE capacity < " + capacity

cursor.execute(query)

records = cursor.fetchall()


for t in records:
    print(t)



# Good. Do it this way

query = "SELECT * FROM course WHERE semester=%s AND name=%s"
cursor.execute(query, (semester, course_name))
#
# records = cursor.fetchall()
#
#
# for t in records:
#     print(t)


